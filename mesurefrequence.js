

function mesurefrequence (taille_echantillon, borne) {

    let frequence = {};
    // initialisation à 0 de l'objet tableau des fréquences
    for (let valeur = 0; valeur < borne; valeur++) {
        frequence[valeur]=0 
    }

    // decompte des occurences
    for(let indice = 0; indice < taille_echantillon; indice++) {
        let nombre_aleatoire = Math.floor ( Math.random() * borne );
        frequence[nombre_aleatoire]++;
    }
    
    // initialisation des valeurs de frequence max et min
    let freqmin = taille_echantillon;
    let freqmax = 0;
    let valeur = 0;

    // parcours de l'objet tableau des fréquences pour determiner les frequences min et max
    for (valeur in frequence) {
        if (frequence[valeur] < freqmin){
            freqmin = frequence[valeur];
        }
        if (frequence[valeur] > freqmax){
            freqmax = frequence[valeur];
        }
    }

    let resultats = {"frequencemin" : freqmin, "frequencemax" : freqmax};
    return resultats;
}

function plusieursmesures (taille_echantillon, borne, nb_tirages) {

    //initialisation des variables
    let ecart_min = taille_echantillon
    let ecart_max = 0
    let ecart_total = 0

    //realisation des tirages
    for(let indice = 0; indice < nb_tirages; indice++) {

        let resultat = mesurefrequence (taille_echantillon, borne);
        let ecart = resultat.frequencemax - resultat.frequencemin;

        // mise a jour des indicateurs
        ecart_total += ecart;
        ecart > ecart_max ? ecart_max = ecart : ecart_max = ecart_max ;
        ecart < ecart_min ? ecart_min = ecart : ecart_min = ecart_min ;
    }

    let ecart_moyen = ecart_total/nb_tirages

    let resultats = {"ecartmin" : ecart_min, "ecartmax" : ecart_max, "ecartmoy" : ecart_moyen};
    return resultats;
}


function PlusieursTaillesEchantillons(tailles, borne) {

    let nb_tirages = tailles.length;
    let resultats = [];

    for(let indice = 0; indice < nb_tirages; indice++) {
        let echantillon_actuel = tailles[indice]
        resultat = mesurefrequence (echantillon_actuel, borne)
        let frequence_min = resultat.frequencemin/echantillon_actuel
        let frequence_max = resultat.frequencemax/echantillon_actuel
        let resultat_local = {"taille d'echantillon" : echantillon_actuel, "frequencemin" : frequence_min, "frequencemax" : frequence_max}
        resultats.push(resultat_local)
    }

    return resultats
}
